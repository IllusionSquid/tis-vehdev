--[[
    tis-vehdev
    Copyright (C) 2022  The Illusion Squid

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

local menu = MenuV:CreateMenu(false, 'Car Dev Menu', 'topright', 155, 0, 155, 'size-125', 'none', 'menuv', 'test')

local vehicle = nil

local attributes = { -- Descriptions taken from: https://forums.gta5-mods.com/topic/3842/tutorial-handling-meta
    {label = "fMass", description = "The weight of the vehicle. Values should be given in Kilograms."},
    {label = "fInitialDragCoeff", description = "Sets the drag coefficient of the vehicle. Increase to simulate aerodynamic drag."},
    {label = "fDriveBiasFront", description = "This is used to determine whether a vehicle is front, rear, or four wheel drive. \n- 0.0 means that the vehicle is rear wheel drive. \n- 1.0 means that the vehicle is front wheel drive.\n- 0.5 give both front and rear axles equal force, being the perfect 4WD."},
    {label = "nInitialDriveGears", description = "Obviously, this line determines how many forward speeds/gears a vehicle's transmission contains."},
    {label = "fInitialDriveForce", description = "This modifies the game's calculation of drive force (from the output of the transmission).\n- 1.0 uses drive force calculation unmodified.\n- Values less than 1.0 will in effect give the vehicle less drive force.\n- Values greater than 1.0 will produce more drive force."},
    {label = "fDriveInertia", description = "Describes how fast an engine will rev.\nBigger values = quicker Redline (maximum engine speed)"},
    {label = "fClutchChangeRateScaleUpShift", description = "Clutch speed multiplier on up shifts."},
    {label = "fClutchChangeRateScaleDownShift", description = "Clutch speed multiplier on down shifts."},
    {label = "InitialDriveMaxFlatVel", description = "Setting this value DOES NOT guarantee the vehicle will reach the given speed.\nNOTE: Multiply the number in the file by 0.82 to get the speed in mph."},
    {label = "fBrakeForce", description = "Obvious one. Multiplies the game's calculation of deceleration.\n- Bigger numbers = harder braking."},
    {label = "fBrakeBiasFront", description = "This line controls the distribution of braking force between the front and rear axles.\nSimular to fDriveBiasFront"},
    {label = "fHandBrakeForce", description = "Another obvious one. Braking power of the handbrake."},
    {label = "fSteeringLock", description = "This multiplies the game's calculation of the angle of the steer wheel will turn while at full turn.\nSteering lock is directly related to over/under-steer."},
    {label = "fTractionCurveMax", description = "Cornering grip of the vehicle as a multiplier of the tire surface friction."},
    {label = "fTractionCurveMin", description = "Accelerating/braking grip of the vehicle as a multiplier of the tire surface friction."},
    {label = "fTractionCurveLateral", description = "Shape of lateral traction curve."},
    {label = "fTractionSpringDeltaMax", description = "This value determines at what distance above the ground the car will lose traction."},
    {label = "fLowSpeedTractionLossMult", description = "How much traction is reduced at low speed.\n- 0.0 means normal traction. It affects mainly car burnout when pressing gas (W/UP).\n- Decreasing value will cause less burnout, less sliding at start.\n- Higher value will cause more burnout."},
    {label = "fCamberStiffnesss", description = "This value modify the grip of the car when you're drifting.\n- More than 0 make the car sliding on the same angle you're drifting.\n- Less than 0 make your car oversteer"},
    {label = "fTractionBiasFront", description = "Determines the distribution of traction from front to rear.\n- 0.01 = only rear axle has traction. \n- 0.99 = only front axle has traction.\n- 0.5 = both axles have equal traction. \n- Entering a value of 0.0 or 1.0 causes the vehicle to have no traction."},
    {label = "fTractionLossMult", description = "Affects how much grip is changed when driving on asphalt and mud\nHigher values make the car less responsive and prone to sliding. "},
    {label = "fSuspensionForce", description = "Affects how strong suspension is.\n1/(Force × Wheels) = Lower limit for zero force at full extension."},
    {label = "fSuspensionCompDamp", description = "Damping during strut compression.\nBigger values = stiffer."},
    {label = "fSuspensionReboundDamp", description = "Damping during strut rebound.\nBigger values = stiffer."},
    {label = "fSuspensionUpperLimit", description = "Visual limit of how far can wheels move up / down from original position."},
    {label = "fSuspensionLowerLimit", description = "Visual limit of how far can wheels move up / down from original position."},
    {label = "fSuspensionRaise", description = "The amount that the suspension raises the body off the wheels."},
    {label = "fSuspensionBiasFront", description = "This value determines which suspension is stronger, front or rear.\nIf value is above 0.50 then front is stiffer, when below, rear is stiffer."},
    {label = "fAntiRollBarForce", description = "Larger Numbers = less body roll."},
    {label = "fAntiRollBarBiasFront", description = "The bias between front and rear for the anti-roll bar\n- 0 = front\n- 1 = rear"},
    {label = "fRollCentreHeightFront", description = "- Larger Numbers = less rollovers.\nValues: (Recommended) -0.15 to 0.15."},
    {label = "fRollCentreHeightRear", description = "This value modify the weight transmission during an acceleration between the front and rear. high positive value can make your car able to do wheelies.\n- Larger Numbers = less rollovers."},
}

local handlingIndex = { -- This is the complete handling list
    "handlingName",
    "fMass",
    "fInitialDragCoeff",
    "fPercentSubmerged",
    "vecCentreOfMassOffset",
    "vecInertiaMultiplier",
    "fDriveBiasFront",
    "nInitialDriveGears",
    "fInitialDriveForce",
    "fDriveInertia",
    "fClutchChangeRateScaleUpShift",
    "fClutchChangeRateScaleDownShift",
    "fInitialDriveMaxFlatVel",
    "fBrakeForce",
    "fBrakeBiasFront",
    "fHandBrakeForce",
    "fSteeringLock",
    "fTractionCurveMax",
    "fTractionCurveMin",
    "fTractionCurveLateral",
    "fTractionSpringDeltaMax",
    "fLowSpeedTractionLossMult",
    "fCamberStiffnesss",
    "fTractionBiasFront",
    "fTractionLossMult",
    "fSuspensionForce",
    "fSuspensionCompDamp",
    "fSuspensionReboundDamp",
    "fSuspensionUpperLimit",
    "fSuspensionLowerLimit",
    "fSuspensionRaise",
    "fSuspensionBiasFront",
    "fAntiRollBarForce",
    "fAntiRollBarBiasFront",
    "fRollCentreHeightFront",
    "fRollCentreHeightRear",
    "fCollisionDamageMult",
    "fWeaponDamageMult",
    "fDeformationDamageMult",
    "fEngineDamageMult",
    "fPetrolTankVolume",
    "fOilVolume",
    "fSeatOffsetDistX",
    "fSeatOffsetDistY",
    "fSeatOffsetDistZ",
    "nMonetaryValue",
}
RegisterNetEvent('tis-vehdev:client:devcar')
AddEventHandler('tis-vehdev:client:devcar', function(encr)
    TriggerServerEvent("tis-vehdev:server:fix", encr)
    vehicle = GetVehiclePedIsIn(GetPlayerPed(-1))
    if vehicle ~= nil and vehicle ~= 0   then
        OpenCarDevMenu()
    else
        BeginTextCommandThefeedPost("STRING")
        AddTextComponentSubstringPlayerName("Hello " .. GetPlayerName(PlayerId()) .. ". You must be in a vehicle for this menu to work.")
        EndTextCommandThefeedPostTicker(true, true)
    end
end)

RegisterNetEvent('tis-vehdev:client:copyhandling')
AddEventHandler('tis-vehdev:client:copyhandling', function(encr)
    TriggerServerEvent("tis-vehdev:server:fix", encr)
    vehicle = GetVehiclePedIsIn(GetPlayerPed(-1))
    if vehicle ~= nil and vehicle ~= 0   then
        CopyHandlingMeta()
    else
        BeginTextCommandThefeedPost("STRING")
        AddTextComponentSubstringPlayerName("Hello " .. GetPlayerName(PlayerId()) .. ". You must be in a vehicle for this menu to work.")
        EndTextCommandThefeedPostTicker(true, true)
    end
end)

function OpenCarDevMenu()
    MenuV:CloseMenu(menu) -- Prevent a shit ton of menus appearing
    MenuV:OpenMenu(menu)
    menu:ClearItems()
    for k, v in pairs(attributes) do
        CreateCarDevButton(v)
    end
end

function CreateCarDevButton(attribute)
    local handlingFunctions = GetHandlingFunctions(attribute)
    local val = handlingFunctions.getter(vehicle, "CHandlingData", attribute.label)

    local button = menu:AddButton({
        icon = '',
        label = attribute.label..' - '..val,
        value = '',
        description = attribute.description,
        select = function(btn)
            local newVal = LocalInputInt(attribute.label..' value', 255, val)
            print(newVal)
            handlingFunctions.setter(vehicle, "CHandlingData", attribute.label, newVal)

            ModifyVehicleTopSpeed(vehicle,1)
            OpenCarDevMenu()
        end
    })
end

function GetHandlingFunctions(attribute)
    local type = string.sub(attribute.label, 1, 1)
    if type == "f" or attribute.label == "InitialDriveMaxFlatVel" then
        return {setter = SetVehicleHandlingFloat, getter = GetVehicleHandlingFloat}
    elseif type == "n" then
        return {setter = SetVehicleHandlingInt, getter = GetVehicleHandlingInt}
    elseif type == "v" then
        return {setter = SetVehicleHandlingVector, getter = GetVehicleHandlingVector}
    end
end

function LocalInputInt(text, number, windows)
    AddTextEntry("TIS-VEHDEV", text)
    DisplayOnscreenKeyboard(1, "TIS-VEHDEV", "", windows or "", "", "", "", number or 30)
    while (UpdateOnscreenKeyboard() == 0) do
      DisableAllControlActions(0)
      Wait(0)
    end
    if (GetOnscreenKeyboardResult()) then
      local result = GetOnscreenKeyboardResult()
      return tonumber(result)
    end
end


function CopyHandlingMeta()
    local handlingLines = ""

    for k, v in pairs(handlingIndex) do
        local type = string.sub(v, 1, 1)
        if type == "f" then
            handlingLines = string.format("%s      <%s value=\"%f\" />\n", handlingLines, v, GetVehicleHandlingFloat(vehicle, "CHandlingData", v))
        elseif type == "n" then
            handlingLines = string.format("%s      <%s value=\"%d\" />\n", handlingLines, v, GetVehicleHandlingInt(vehicle, "CHandlingData", v))
        elseif type == "v" then
            local handlingVector = GetVehicleHandlingVector(vehicle, "CHandlingData", v)
            handlingLines = string.format("%s      <%s x=\"%f\" y=\"%f\" z=\"%f\" />\n", handlingLines, v, handlingVector.x, handlingVector.y, handlingVector.z)
        elseif type == "n" then
            handlingLines = string.format("%s      <%s value=\"%d\" />\n", handlingLines, v, GetVehicleModelMonetaryValue(GetEntityModel(vehicle)))
        end
    end

    SendNUIMessage({
        copy = handlingLines
    })
end