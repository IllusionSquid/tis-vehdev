resource_manifest_version "44febabe-d386-4d18-afbe-5e627f4af937"

description "A tool to help with adjusting handling of a car"
author "The Illusion Squid"

ui_page 'html/index.html'

client_scripts {
    '@menuv/menuv.lua',
	"client/main.lua"
}

server_scripts {
	"server/main.lua"
}

files {
	'html/index.html',
	'html/jquery.js',
	'html/init.js',
}

dependencies {
    'menuv'
}