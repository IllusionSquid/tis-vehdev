# Copyright notice
Copyright 2022, The Illusion Squid, All rights reserved.

```
    tis-vehdev
    Copyright (C) 2022  The Illusion Squid

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

- This copyright excludes everything that is inside the `html` folder, I do not claim property over this.

# Dependencies
- **[MenuV](https://github.com/ThymonA/menuv)** 

# Security
This project contains the "Cheap Encryption" method designed by me to add a very poor additional security layer. It is designed to work against people that do **not** know any coding. Anyone with experience will most likely be able to grasp and overcome the concept. Any feedback on the method is largely appreciated. Contact on Discord: `The Illusion Squid®#4964`

# NOTICE
There is a ***VERY HIGH*** chance that some values that are copied are **not** correct. Please use caution when using the copied handling lines. You might be better off copying the edited handling values yourself.

# Installation
Download and drop inside recourse folder and add the line `start tis-vehdev` in your `server.cfg`.

# Usage
Add ace for perms to use:
```
add_ace identifier.steam:hex command.devcar allow
add_ace identifier.steam:hex command.copyhandling allow
```

This script adds 2 commands:
- `devcar` | This opens the menu
- `copyhandling` | This copies the handling to your clipboard
