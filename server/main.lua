--[[
    tis-vehdev
    Copyright (C) 2022  The Illusion Squid

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

local Encryptions = {}
local charset = {}  do -- [0-9a-zA-Z]
    for c = 48, 57  do table.insert(charset, string.char(c)) end
    for c = 65, 90  do table.insert(charset, string.char(c)) end
    for c = 97, 122 do table.insert(charset, string.char(c)) end
end

local function randomString(length)
    if not length or length <= 0 then return '' end
    math.randomseed(os.clock())
    return randomString(length - 1) .. charset[math.random(1, #charset)]
end

RegisterCommand("devcar", function(source, args, rawCommand)
    Encryptions[source] = randomString(8)
    TriggerClientEvent("tis-vehdev:client:devcar", source, Encryptions[source])
end, true)

RegisterCommand("copyhandling", function(source, args, rawCommand)
    Encryptions[source] = randomString(8)
    TriggerClientEvent("tis-vehdev:client:copyhandling", source, Encryptions[source])
end, true)

RegisterServerEvent("tis-vehdev:server:fix")
AddEventHandler("tis-vehdev:server:fix", function(encrypt)
    if Encryptions[source] ~= encrypt then
        DropPlayer(source, "Kindly stop trying a cheap hack - Squid")
    end
end)